package fr.pixelmon_france.daem0ns.pokedbank.commands;

import ca.landonjw.gooeylibs2.implementation.tasks.Task;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonFactory;
import com.pixelmonmod.pixelmon.api.util.helpers.TextHelper;
import com.pixelmonmod.pixelmon.command.PixelCommand;
import fr.pixelmon_france.daem0ns.pokedbank.PokeBank;
import fr.pixelmon_france.daem0ns.pokedbank.database.Statements;
import fr.pixelmon_france.daem0ns.pokedbank.ui.InventoryProvider;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandSource;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.util.Util;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.server.permission.PermissionAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PokeDBankCommand extends PixelCommand {

    public PokeDBankCommand(CommandDispatcher<CommandSource> dispatcher) {
        super(dispatcher, "pokedbank", "/pokebank <reload|applySpec> [username|specs,separated,with,comas] [specs,separated,with,comas]", 0);
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("pokebank", "pokedbank", "pb");
    }

    @Override
    public void execute(CommandSource sender, String[] args) throws CommandException, CommandSyntaxException {

        if (args.length == 0) {
            if (!(sender.asPlayer() instanceof ServerPlayerEntity)) {
                sender.getServer().sendMessage(TextHelper.colour(PokeBank.instance.locale.onlyPlayers), Util.DUMMY_UUID);
                return;
            }

            ServerPlayerEntity player = sender.asPlayer();

            if (PokeBank.instance.config.useDatabase) {
                if (Statements.isLocked(player)) {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.multipleServersAccess), Util.DUMMY_UUID);
                    return;
                } else {
                    Statements.lockBank(player);
                }
            }

            new Task.TaskBuilder().execute(() -> {
                        try {
                            InventoryProvider.openPokebankInventory(player);
                        } catch (CommandSyntaxException e) {
                            e.printStackTrace();
                        }
                    }).delay(PokeBank.instance.config.openUIDelay * 20L)
                    .build();
        } else if (args.length == 1) {
            if (!PermissionAPI.hasPermission(sender.asPlayer(), "pokebank.admin")) {
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.noPermission), Util.DUMMY_UUID);
                return;
            }

            if (args[0].equalsIgnoreCase("reload")) {
                try {
                    PokeBank.instance.reloadConfig();
                    PokeBank.instance.loadPlayerData();
                    sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.reloaded), Util.DUMMY_UUID);
                } catch (IOException e) {
                    sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.reloadError), Util.DUMMY_UUID);
                    e.printStackTrace();
                }
            } else {
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.wrongUsage), Util.DUMMY_UUID);
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.usage), Util.DUMMY_UUID);
            }
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("applyspec")) {
                String[] specs = args[1].split(",");
                List<String> ll = new ArrayList<>();

                PokeBank.instance.playerData.playerPokemon.forEach((uuid, l) -> {
                    l.forEach(s -> {
                        try {
                            Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(s));
                            for (String ss : specs) {
                                pkmn.addFlag(ss);
                            }
                            if (pkmn == null) {
                                ll.add(s);
                            } else {
                                CompoundNBT pokemon = new CompoundNBT();
                                pkmn.writeToNBT(pokemon);
                                ll.add(String.valueOf(pokemon));
                            }
                        } catch (CommandSyntaxException e) {
                            e.printStackTrace();
                        }

                    });
                    PokeBank.instance.playerData.playerPokemon.put(uuid, ll);
                });
                PokeBank.instance.savePlayerData();
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.editedBank), Util.DUMMY_UUID);
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.appliedSpecs
                        .replace("%specs%", Arrays.toString(specs)
                                .replace(",", "\", \"")
                                .replace("[", "\"")
                                .replace("]", "\""))),
                        Util.DUMMY_UUID
                );
            } else {
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.wrongUsage), Util.DUMMY_UUID);
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.usage), Util.DUMMY_UUID);
            }
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("applyspec")) {
                String playerName = args[1];
                ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayerByUsername(playerName);

                if (player == null) {
                    sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.noPlayer), Util.DUMMY_UUID);
                    return;
                }

                String[] specs = args[2].split(",");
                List<String> ll = new ArrayList<>();
                List<String> mon = PokeBank.instance.playerData.playerPokemon.get(player.getUniqueID());
                mon.forEach(s -> {
                    try {
                        Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(s));
                        for (String ss : specs) {
                            pkmn.addFlag(ss);
                            CompoundNBT pokemon = new CompoundNBT();
                            pkmn.writeToNBT(pokemon);
                            ll.add(String.valueOf(pokemon));
                        }
                    } catch (CommandSyntaxException e) {
                        e.printStackTrace();
                    }
                });
                PokeBank.instance.playerData.playerPokemon.put(player.getUniqueID(), ll);
                PokeBank.instance.savePlayerData();
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.editedSpecificBank
                        .replace("%player%", playerName)
                ), Util.DUMMY_UUID);
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.appliedSpecs
                        .replace("%specs%", Arrays.toString(specs)
                                .replace(",", "\", \"")
                                .replace("[", "\"")
                                .replace("]", "\""))),
                        Util.DUMMY_UUID
                );
            } else {
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.wrongUsage), Util.DUMMY_UUID);
                sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.usage), Util.DUMMY_UUID);
            }
        } else {
            sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.wrongUsage), Util.DUMMY_UUID);
            sender.asPlayer().sendMessage(TextHelper.colour(PokeBank.instance.locale.usage), Util.DUMMY_UUID);
        }
    }
}
