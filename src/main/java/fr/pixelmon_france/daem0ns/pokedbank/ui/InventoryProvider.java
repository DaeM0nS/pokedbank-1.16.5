package fr.pixelmon_france.daem0ns.pokedbank.ui;

import ca.landonjw.gooeylibs2.api.UIManager;
import ca.landonjw.gooeylibs2.api.button.Button;
import ca.landonjw.gooeylibs2.api.button.GooeyButton;
import ca.landonjw.gooeylibs2.api.page.GooeyPage;
import ca.landonjw.gooeylibs2.api.template.types.ChestTemplate;
import ca.landonjw.gooeylibs2.implementation.tasks.Task;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonFactory;
import com.pixelmonmod.pixelmon.api.pokemon.stats.BattleStatsType;
import com.pixelmonmod.pixelmon.api.registries.PixelmonPalettes;
import com.pixelmonmod.pixelmon.api.registries.PixelmonSpecies;
import com.pixelmonmod.pixelmon.api.storage.PlayerPartyStorage;
import com.pixelmonmod.pixelmon.api.storage.StorageProxy;
import com.pixelmonmod.pixelmon.api.util.helpers.SpriteItemHelper;
import com.pixelmonmod.pixelmon.api.util.helpers.TextHelper;
import fr.pixelmon_france.daem0ns.pokedbank.PokeBank;
import fr.pixelmon_france.daem0ns.pokedbank.database.Statements;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.util.Tuple;
import net.minecraft.util.Util;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class InventoryProvider {

    private static final Button blackGlass = GooeyButton.of(new ItemStack(Blocks.BLACK_STAINED_GLASS_PANE));
    private static final Button redGlass = GooeyButton.of(new ItemStack(Blocks.RED_STAINED_GLASS_PANE));
    private static final Button whiteGlass = GooeyButton.of(new ItemStack(Blocks.WHITE_STAINED_GLASS_PANE));
    private static final Button emptyPaperBank = GooeyButton.of(new ItemStack(Items.PAPER).setDisplayName(TextHelper.colour(PokeBank.instance.locale.emptyBankGUI)));
    private static final Button emptyPaperTeam = GooeyButton.of(new ItemStack(Items.PAPER).setDisplayName(TextHelper.colour(PokeBank.instance.locale.emptyPartyGUI)));

    public static void openPokebankInventory(ServerPlayerEntity player) throws CommandSyntaxException {
        Tuple<ArrayList<Button>, ArrayList<Button>> buttons = getButtonList(player);

        ArrayList<Button> storedList = buttons.getA();
        ArrayList<Button> partyList = buttons.getB();

        ChestTemplate template = ChestTemplate.builder(6)
                .row(0, redGlass)
                .row(1, blackGlass)
                .row(2, whiteGlass)
                .row(3, redGlass)
                .row(4, blackGlass)
                .row(5, whiteGlass)
                .build();

        if (storedList.size() <= 9) {
            switch (storedList.size()) {
                case 0: {
                    template.set(2, 4, emptyPaperBank);
                    template.update();
                    break;
                }
                case 1: {
                    template.set(2, 4, storedList.get(0));
                    template.update();
                    break;
                }
                case 2: {
                    template.set(2, 3, storedList.get(0));
                    template.set(2, 5, storedList.get(1));
                    template.update();
                    break;
                }
                case 3: {
                    template.set(2, 2, storedList.get(0));
                    template.set(2, 4, storedList.get(1));
                    template.set(2, 6, storedList.get(2));
                    template.update();
                    break;
                }
                case 4: {
                    template.set(1, 4, storedList.get(0));
                    template.set(1, 6, storedList.get(1));
                    template.set(3, 4, storedList.get(2));
                    template.set(3, 6, storedList.get(3));
                    template.update();
                    break;
                }
                case 5: {
                    template.set(1, 3, storedList.get(0));
                    template.set(1, 5, storedList.get(1));
                    template.set(2, 4, storedList.get(2));
                    template.set(3, 3, storedList.get(3));
                    template.set(3, 5, storedList.get(4));
                    template.update();
                    break;
                }
                case 6: {
                    template.set(1, 2, storedList.get(0));
                    template.set(1, 4, storedList.get(1));
                    template.set(1, 6, storedList.get(2));
                    template.set(3, 2, storedList.get(3));
                    template.set(3, 4, storedList.get(4));
                    template.set(3, 6, storedList.get(5));
                    template.update();
                    break;
                }
                case 7: {
                    template.set(1, 2, storedList.get(0));
                    template.set(1, 4, storedList.get(1));
                    template.set(1, 6, storedList.get(2));
                    template.set(2, 4, storedList.get(3));
                    template.set(3, 2, storedList.get(4));
                    template.set(3, 4, storedList.get(5));
                    template.set(3, 6, storedList.get(6));
                    template.update();
                    break;
                }
                case 8: {
                    template.set(1, 1, storedList.get(0));
                    template.set(1, 3, storedList.get(1));
                    template.set(1, 5, storedList.get(2));
                    template.set(1, 7, storedList.get(3));
                    template.set(3, 1, storedList.get(4));
                    template.set(3, 3, storedList.get(5));
                    template.set(3, 5, storedList.get(6));
                    template.set(3, 7, storedList.get(7));
                    template.update();
                    break;
                }
                case 9: {
                    template.set(1, 1, storedList.get(0));
                    template.set(1, 3, storedList.get(1));
                    template.set(1, 5, storedList.get(2));
                    template.set(1, 7, storedList.get(3));
                    template.set(2, 4, storedList.get(4));
                    template.set(3, 1, storedList.get(5));
                    template.set(3, 3, storedList.get(6));
                    template.set(3, 5, storedList.get(7));
                    template.set(3, 7, storedList.get(8));
                    template.update();
                    break;
                }
            }
        }

        switch (partyList.size()) {
            case 0: {
                template.set(5, 4, emptyPaperTeam);
                template.update();
                break;
            }
            case 1: {
                template.set(5, 4, partyList.get(0));
                template.update();
                break;
            }
            case 2: {
                template.set(5, 3, partyList.get(0));
                template.set(5, 5, partyList.get(1));
                template.update();
                break;
            }
            case 3: {
                template.set(5, 2, partyList.get(0));
                template.set(5, 4, partyList.get(1));
                template.set(5, 6, partyList.get(2));
                template.update();
                break;
            }
            case 4: {
                template.set(5, 1, partyList.get(0));
                template.set(5, 3, partyList.get(1));
                template.set(5, 5, partyList.get(2));
                template.set(5, 7, partyList.get(3));
                template.update();
                break;
            }
            case 5: {
                template.set(5, 0, partyList.get(0));
                template.set(5, 2, partyList.get(1));
                template.set(5, 4, partyList.get(2));
                template.set(5, 6, partyList.get(3));
                template.set(5, 8, partyList.get(4));
                template.update();
                break;
            }
            case 6: {
                template.set(5, 1, partyList.get(0));
                template.set(5, 2, partyList.get(1));
                template.set(5, 3, partyList.get(2));
                template.set(5, 5, partyList.get(3));
                template.set(5, 6, partyList.get(4));
                template.set(5, 7, partyList.get(5));
                template.update();
                break;
            }
        }

        GooeyPage page = GooeyPage.builder()
                .title(PokeBank.instance.locale.PokeBankGUITitle)
                .template(template)
                .onClose(() -> {
                    if (PokeBank.instance.config.useDatabase)
                        new Task.TaskBuilder().execute(() ->
                                        Statements.unlockBank(player))
                                .delay(PokeBank.instance.config.unlockCooldown * 20L)
                                .build();
                }).build();

        UIManager.openUIPassively(player, page, 0, TimeUnit.MILLISECONDS);
    }

    private static Tuple<ArrayList<Button>, ArrayList<Button>> getButtonList(ServerPlayerEntity player) throws CommandSyntaxException {
        PlayerPartyStorage playerStorage = StorageProxy.getParty(player.getUniqueID());
        playerStorage.retrieveAll();

        ArrayList<Button> storedList = new ArrayList<>();
        ArrayList<Button> partyList = new ArrayList<>(Arrays.asList(null, null, null, null, null, null));

        if (PokeBank.instance.config.useDatabase) {
            ArrayList<String> storedPokemon = new ArrayList<>();
            ArrayList<Integer> idStoredPokemon = new ArrayList<>();
            Statements.getPlayerPokemon(player.getUniqueID()).forEach((id, pokemon) -> {
                storedPokemon.add(pokemon);
                idStoredPokemon.add(id);
            });

            int slot;
            for (slot = 0; slot < storedPokemon.size(); slot++) {
                CompoundNBT pokemonNBT = JsonToNBT.getTagFromJson(storedPokemon.get(slot));
                Pokemon p = PokemonFactory.create(pokemonNBT);
                Optional<ArrayList<String>> lore = getLoreForNBT(p, true);
                if (lore.isPresent()) {
                    int finalSlot = slot;
                    storedList.add(GooeyButton.builder()
                            .display(SpriteItemHelper.getPhoto(p))
                            .title(p.getDisplayName())
                            .lore(lore.get())
                            .onClick(e -> {
                                if (Statements.existId(idStoredPokemon.get(finalSlot))) {
                                    removeFromStorage(player, storedPokemon, playerStorage, finalSlot, pokemonNBT);
                                    try {
                                        openPokebankInventory(player);
                                    } catch (CommandSyntaxException ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.notInDatabase), Util.DUMMY_UUID);
                                }
                            }).build());
                } else {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.unableToConvert), Util.DUMMY_UUID);
                }
            }

            for (int i = 0; i < 6; i++) {
                if (playerStorage.get(i) == null) {
                    continue;
                }
                Pokemon p = playerStorage.get(i);
                Optional<ArrayList<String>> lore = getLoreForNBT(p, false);
                if (lore.isPresent()) {
                    int finalSlot = i;

                    partyList.set(i, GooeyButton.builder()
                            .display(SpriteItemHelper.getPhoto(p))
                            .title(p.getDisplayName())
                            .lore(lore.get())
                            .onClick(e -> {
                                addToStorage(player, storedPokemon, playerStorage, finalSlot);
                                try {
                                    openPokebankInventory(player);
                                } catch (CommandSyntaxException ex) {
                                    ex.printStackTrace();
                                }
                            }).build());
                } else {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.unableToConvert), Util.DUMMY_UUID);
                }
            }

        } else {
            ArrayList<String> storedPokemon = new ArrayList<>();
            if (PokeBank.instance.playerData.playerPokemon.containsKey(player.getUniqueID())) {
                storedPokemon.addAll(PokeBank.instance.playerData.playerPokemon.get(player.getUniqueID()));
            }

            int slot;
            for (slot = 0; slot < storedPokemon.size(); slot++) {
                CompoundNBT pokemonNBT = JsonToNBT.getTagFromJson(storedPokemon.get(slot));
                Pokemon p = PokemonFactory.create(pokemonNBT);
                Optional<ArrayList<String>> lore = getLoreForNBT(p, true);
                if (lore.isPresent()) {
                    int finalSlot = slot;
                    storedList.add(GooeyButton.builder()
                            .display(SpriteItemHelper.getPhoto(p))
                            .title(p.getDisplayName())
                            .lore(lore.get())
                            .onClick(e -> {
                                removeFromStorage(player, storedPokemon, playerStorage, finalSlot, pokemonNBT);
                                try {
                                    openPokebankInventory(player);
                                } catch (CommandSyntaxException ex) {
                                    ex.printStackTrace();
                                }
                            }).build());
                } else {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.unableToConvert), Util.DUMMY_UUID);
                }
            }

            for (int i = 0; i < 6; i++) {
                if (playerStorage.get(i) == null) {
                    continue;
                }
                Pokemon p = playerStorage.get(i);
                Optional<ArrayList<String>> lore = getLoreForNBT(p, false);
                if (lore.isPresent()) {
                    int finalSlot = i;

                    partyList.set(i, GooeyButton.builder()
                            .display(SpriteItemHelper.getPhoto(p))
                            .title(p.getDisplayName())
                            .lore(lore.get())
                            .onClick(e -> {
                                addToStorage(player, storedPokemon, playerStorage, finalSlot);
                                try {
                                    openPokebankInventory(player);
                                } catch (CommandSyntaxException ex) {
                                    ex.printStackTrace();
                                }
                            }).build());
                } else {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.unableToConvert), Util.DUMMY_UUID);
                }
            }
        }
        return new Tuple<>(storedList, partyList);
    }

    private static Optional<ArrayList<String>> getLoreForNBT(Pokemon p, boolean stored) {
        if (p == null) {
            return Optional.empty();
        }
        if (p.isEgg()) {
            return Optional.empty();
        }
        ArrayList<String> lore = new ArrayList<>();
        lore.add("Here's some additional info:");
        addLoreStat(lore, "EVs", p.getEVs().getStat(BattleStatsType.HP) + p.getEVs().getStat(BattleStatsType.ATTACK) + p.getEVs().getStat(BattleStatsType.DEFENSE) + p.getEVs().getStat(BattleStatsType.SPEED) + p.getEVs().getStat(BattleStatsType.SPECIAL_ATTACK) + p.getEVs().getStat(BattleStatsType.SPECIAL_DEFENSE), 510);
        addLoreStat(lore, "IVs", p.getIVs().getStat(BattleStatsType.HP) + p.getIVs().getStat(BattleStatsType.ATTACK) + p.getIVs().getStat(BattleStatsType.DEFENSE) + p.getIVs().getStat(BattleStatsType.SPEED) + p.getIVs().getStat(BattleStatsType.SPECIAL_ATTACK) + p.getIVs().getStat(BattleStatsType.SPECIAL_DEFENSE), 186);
        lore.add("IVs details:");
        addLoreStat(lore, "HP", p.getIVs().getStat(BattleStatsType.HP), 31);
        addLoreStat(lore, "Attack", p.getIVs().getStat(BattleStatsType.ATTACK), 31);
        addLoreStat(lore, "Defence", p.getIVs().getStat(BattleStatsType.DEFENSE), 31);
        addLoreStat(lore, "Special Attack", p.getIVs().getStat(BattleStatsType.SPECIAL_ATTACK), 31);
        addLoreStat(lore, "Special Defence", p.getIVs().getStat(BattleStatsType.SPECIAL_DEFENSE), 31);
        addLoreStat(lore, "Speed", p.getIVs().getStat(BattleStatsType.SPEED), 31);
        lore.add(TextFormatting.GOLD+ "Click here to " + (stored ? "restore" : "store"));


        return Optional.of(lore);
    }

    private static void addLoreStat(ArrayList<String> lore, String name, int current, int max) {
        lore.add(TextFormatting.GOLD + name + ": " + TextFormatting.YELLOW + current + TextFormatting.GOLD + "/" + TextFormatting.YELLOW + max);
    }

    private static void addToStorage(ServerPlayerEntity player, List<String> storedPokemon, PlayerPartyStorage storage, int slotIndex) {
        try {
            if (!PokeBank.instance.config.canAddPokemon) {
                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.storageDisabled), Util.DUMMY_UUID);
                return;
            }
            if (storage.get(slotIndex) == null) {
                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.emptySlot), Util.DUMMY_UUID);
                return;
            }

            HashMap<String, Integer> playerPerms = new HashMap<>();

            playerPerms.put("pokebank.amount", 0);
            playerPerms.put("pokebank.shinyamount", 0);
            playerPerms.put("pokebank.textureamount", 0);
            playerPerms.put("pokebank.specialamount", 0);
            playerPerms.put("pokebank.legendaryamount", 0);
            playerPerms.put("pokebank.beastamount", 0);

            PokeBank.instance.config.nodes.forEach((k, v) -> v.forEach((meta, amount) -> {
                if (PermissionAPI.hasPermission(player, k)) {
                    if (!playerPerms.containsKey(meta)) {
                        playerPerms.put(meta, amount);
                    } else {
                        int storedAmount = playerPerms.get(meta);
                        if (storedAmount < amount) {
                            playerPerms.put(meta, amount);
                        }
                    }
                }
            }));

            int maxStorage = Integer.min(playerPerms.get("pokebank.amount"), 9);
            if (storedPokemon.size() >= maxStorage) {
                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxStored.replace("%amount%", String.valueOf(maxStorage))), Util.DUMMY_UUID);
                return;
            }

            if (PokeBank.instance.config.pokemonBlacklist.contains(storage.get(slotIndex).getSpecies().getName().toLowerCase())) {
                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.blacklistedPokemon), Util.DUMMY_UUID);
                return;
            }

            for (String spec : PokeBank.instance.config.specsBlacklist) {
                if (storage.get(slotIndex).hasFlag(spec)) {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.blacklistedSpec
                            .replace("%spec%", spec)
                    ), Util.DUMMY_UUID);
                    return;
                }
            }


            if (storage.get(slotIndex).getHeldItem().getItem() != Items.AIR) {
                if (PokeBank.instance.config.canAddPokemonWithItem) {
                    if (PokeBank.instance.config.itemBlacklist.size() != 0) {
                        for (String item : PokeBank.instance.config.itemBlacklist) {
                            if (storage.get(slotIndex).getHeldItem().getItem().getTranslationKey().toLowerCase().contains(item.toLowerCase())) {
                                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.blacklistedHeldItem), Util.DUMMY_UUID);
                                return;
                            }
                        }
                    }
                } else {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.withHeldItem), Util.DUMMY_UUID);
                    return;
                }
            }

            if (!PokeBank.instance.config.allowFusedPokemons) {
                if (storage.get(slotIndex).getSpecies().is(PixelmonSpecies.KYUREM) || (storage.get(slotIndex).getSpecies().is(PixelmonSpecies.NECROZMA)  && !storage.get(slotIndex).getForm().isDefault())) {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.storedFusedPokemon), Util.DUMMY_UUID);
                    return;
                }
            }

            int maxShStorage = Integer.min(playerPerms.get("pokebank.shinyamount"), 9);
            int maxShStored = 0;
            for (String p : storedPokemon) {
                if (PokemonFactory.create(JsonToNBT.getTagFromJson(p)).isShiny()) {
                    maxShStored++;
                }
            }
            if (storage.get(slotIndex).isShiny()) {
                if (maxShStored >= maxShStorage) {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxShiniesStored
                            .replace("%amount%", String.valueOf(maxShStorage))), Util.DUMMY_UUID
                    );
                    return;
                }
            }

            int maxSTStorage = Integer.min(playerPerms.get("pokebank.textureamount"), 9);
            int maxSTStored = 0;
            for (String p : storedPokemon) {
                Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(p));
                if (!pkmn.getPalette().is(PixelmonPalettes.NONE) && !pkmn.getPalette().is(PixelmonPalettes.SHINY)) {
                    maxSTStored++;
                }
            }
            if (!storage.get(slotIndex).getPalette().is(PixelmonPalettes.NONE) && !storage.get(slotIndex).getPalette().is(PixelmonPalettes.SHINY)) {
                if (maxSTStored >= maxSTStorage) {
                    player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxTexturesStored
                            .replace("%amount%", String.valueOf(maxSTStorage))
                    ), Util.DUMMY_UUID);
                    return;
                }
            }

            if (PokeBank.instance.config.triggerAllSpecialPokemon) {
                int maxSpeStorage = Integer.min(playerPerms.get("pokebank.specialamount"), 9);
                int maxSpeStored = 0;
                for (String p : storedPokemon) {
                    Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(p));
                    if (pkmn.isLegendary() || pkmn.isUltraBeast() || pkmn.isMythical()) {
                        maxSpeStored++;
                    }
                }
                if (storage.get(slotIndex).isLegendary() || storage.get(slotIndex).isUltraBeast() || storage.get(slotIndex).isMythical()) {
                    if (maxSpeStored >= maxSpeStorage) {
                        player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxSpecialsStored
                                .replace("%amount%", String.valueOf(maxSpeStorage))
                        ), Util.DUMMY_UUID);
                        return;
                    }
                }
            } else {
                int maxLegStorage = Integer.min(playerPerms.get("pokebank.legendaryamount"), 9);
                int maxLegStored = 0;
                for (String p : storedPokemon) {
                    Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(p));
                    if (pkmn.isLegendary()) {
                        maxLegStored++;
                    }
                }
                if (storage.get(slotIndex).isLegendary()) {
                    if (maxLegStored >= maxLegStorage) {
                        player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxLegendariesStored
                                .replace("%amount%", String.valueOf(maxLegStorage))
                        ), Util.DUMMY_UUID);
                        return;
                    }
                }

                int maxUbStorage = Integer.min(playerPerms.get("pokebank.beastamount"), 9);
                int maxUBStored = 0;
                for (String p : storedPokemon) {
                    Pokemon pkmn = PokemonFactory.create(JsonToNBT.getTagFromJson(p));
                    if (pkmn.isUltraBeast()) {
                        maxUBStored++;
                    }
                }
                if (storage.get(slotIndex).isUltraBeast()) {
                    if (maxUBStored >= maxUbStorage) {
                        player.sendMessage(TextHelper.colour(PokeBank.instance.locale.maxUltraBeastsStored
                                .replace("%amount%", String.valueOf(maxUbStorage))
                        ), Util.DUMMY_UUID);
                        return;
                    }
                }
            }

            if (storage.countPokemon() == 1) {
                player.sendMessage(TextHelper.colour(PokeBank.instance.locale.noPokemon), Util.DUMMY_UUID);
                return;
            }
            CompoundNBT pokemon = new CompoundNBT();
            storage.get(slotIndex).writeToNBT(pokemon);
            storedPokemon.add(String.valueOf(pokemon));
            storage.set(slotIndex, null);
            if(PokeBank.instance.config.useDatabase) {
                Statements.addPokemon(player, String.valueOf(pokemon));
            }else {
                PokeBank.instance.playerData.playerPokemon.put(player.getUniqueID(), storedPokemon);
                PokeBank.instance.savePlayerData();
            }
        } catch (Exception e) {
            player.sendMessage(TextHelper.colour(PokeBank.instance.locale.storageError), Util.DUMMY_UUID);
            System.out.println("Failed to add to storage");
            e.printStackTrace();
        }
    }

    private static void removeFromStorage(ServerPlayerEntity player, List<String> storedPokemon, PlayerPartyStorage storage, int slotIndex, CompoundNBT pokemonNBT) {
        if (!PokeBank.instance.config.canRemovePokemon) {
            player.sendMessage(TextHelper.colour(PokeBank.instance.locale.disabledWithdrawals), Util.DUMMY_UUID);
            return;
        }
        if (storage.countPokemon() == 6) {
            player.sendMessage(TextHelper.colour(PokeBank.instance.locale.fullParty), Util.DUMMY_UUID);
            return;
        }
        storage.set(storage.getFirstEmptyPosition(), PokemonFactory.create(pokemonNBT));
        storedPokemon.remove(slotIndex);
        if(PokeBank.instance.config.useDatabase) {
            Statements.removePokemon(player, String.valueOf(pokemonNBT));
        }else {
            PokeBank.instance.playerData.playerPokemon.put(player.getUniqueID(), storedPokemon);
            PokeBank.instance.savePlayerData();
        }

        PokeBank.instance.playerData.playerPokemon.put(player.getUniqueID(), storedPokemon);
        PokeBank.instance.savePlayerData();
    }
}
