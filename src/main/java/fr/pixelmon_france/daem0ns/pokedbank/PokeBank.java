package fr.pixelmon_france.daem0ns.pokedbank;

import com.pixelmonmod.pixelmon.api.config.api.yaml.YamlConfigFactory;
import fr.pixelmon_france.daem0ns.pokedbank.commands.PokeDBankCommand;
import fr.pixelmon_france.daem0ns.pokedbank.config.PlayerDataConfig;
import fr.pixelmon_france.daem0ns.pokedbank.config.PokeBankConfig;
import fr.pixelmon_france.daem0ns.pokedbank.config.PokeBankLocale;
import fr.pixelmon_france.daem0ns.pokedbank.database.Database;
import info.pixelmon.repack.org.spongepowered.CommentedConfigurationNode;
import info.pixelmon.repack.org.spongepowered.ConfigurateException;
import info.pixelmon.repack.org.spongepowered.loader.ConfigurationLoader;
import info.pixelmon.repack.org.spongepowered.yaml.YamlConfigurationLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


@Mod("pokedbank")
public class PokeBank {

    private Logger logger = null;
    public Logger getLogger() {
        if (this.logger == null) {
            this.logger = LogManager.getLogger("PokeDBank");
        }
        return this.logger;
    }

    public PokeBankConfig config;
    public PokeBankLocale locale;

    private ConfigurationLoader<CommentedConfigurationNode> playerDataLoader;
    public PlayerDataConfig playerData;
    public CommentedConfigurationNode playerDataNode;


    public static PokeBank instance;

    public PokeBank() {
        instance = this;
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void loadPlayerData() throws ConfigurateException {
        this.playerDataLoader = YamlConfigurationLoader.builder().path(new File("./config/pokedbank", "PlayerData.cfg").toPath()).build();
        this.playerDataNode = this.playerDataLoader.load();
        this.playerData = this.playerDataNode.get(PlayerDataConfig.class, new PlayerDataConfig());
        this.playerDataNode.set(PlayerDataConfig.class, this.playerData);
        this.playerDataLoader.save(this.playerDataNode);
    }

    public void savePlayerData() {
        try {
            this.playerDataNode.set(PlayerDataConfig.class, this.playerData);
            this.playerDataLoader.save(this.playerDataNode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupConfig() throws IOException {
        this.reloadConfig();
        this.loadPlayerData();
        if (this.config.useDatabase) {
            Database.getConnection();
            Database.testCon();
            try {
                Database.createTable();
            } catch (SQLException e) {
                this.logger.error("ERROR WHILE CREATING TABLE!");
                e.printStackTrace();
            }
            try {
                Database.createTableLog();
            } catch (SQLException e) {
                this.logger.error("ERROR WHILE CREATING LOG TABLE!");
                e.printStackTrace();
            }
        }
    }

    public void reloadConfig() {
        try {
            this.config = YamlConfigFactory.getInstance(PokeBankConfig.class);
            this.locale = YamlConfigFactory.getInstance(PokeBankLocale.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) throws IOException {
        this.setupConfig();
    }

    @SubscribeEvent
    public void onServerStopping(FMLServerStoppingEvent event) {
        this.savePlayerData();
    }

    @SubscribeEvent
    public void onRegisterCommands(RegisterCommandsEvent event) {
        new PokeDBankCommand(event.getDispatcher());
    }
}
