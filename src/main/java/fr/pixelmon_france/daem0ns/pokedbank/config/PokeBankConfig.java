package fr.pixelmon_france.daem0ns.pokedbank.config;


import com.pixelmonmod.pixelmon.api.config.api.data.ConfigPath;
import com.pixelmonmod.pixelmon.api.config.api.yaml.AbstractYamlConfig;
import info.pixelmon.repack.org.spongepowered.objectmapping.ConfigSerializable;
import info.pixelmon.repack.org.spongepowered.objectmapping.meta.Setting;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@ConfigSerializable
@ConfigPath("config/pokedbank/PokeDBank.cfg")
public class PokeBankConfig extends AbstractYamlConfig {
    @Setting
    public boolean allowFusedPokemons = true;
    @Setting
    public boolean canAddPokemon = true;
    @Setting
    public boolean canAddPokemonWithItem;
    @Setting
    public List<String> itemBlacklist = Arrays.asList("life_orb", "oran_berry");
    @Setting
    public List<String> pokemonBlacklist = Arrays.asList("pikachu", "ditto");
    @Setting
    public List<String> specsBlacklist = Arrays.asList("untradeable", "unbreedable");
    @Setting
    public boolean canRemovePokemon = true;
    @Setting
    public boolean triggerAllSpecialPokemon = false;
    @Setting
    public String serverName = "MyServer";
    @Setting
    public int openUIDelay = 5;
    @Setting
    public int unlockCooldown = 5;
    @Setting
    public boolean useDatabase = false;
    @Setting
    public String host = "localhost";
    @Setting
    public String database = "MyDatabase";
    @Setting
    public String table = "pokebank";
    @Setting
    public boolean useLogTable = true;
    @Setting
    public String user = "root";
    @Setting
    public String password = "toor";
    @Setting
    public int port = 3306;
    @Setting
    public HashMap<String, HashMap<String, Integer>> nodes = new HashMap<>();

    public PokeBankConfig() {
        HashMap<String, Integer> amountPerType = new HashMap<>();
        amountPerType.put("pokebank.amount", 3);
        amountPerType.put("pokebank.shinyamount", 1);
        amountPerType.put("pokebank.textureamount", 1);
        amountPerType.put("pokebank.specialamount", 1);
        amountPerType.put("pokebank.legendaryamount", 1);
        amountPerType.put("pokebank.beastamount", 1);

        this.nodes.put("pokebank.default", amountPerType);
        this.nodes.put("pokebank.vip", amountPerType);
    }
}