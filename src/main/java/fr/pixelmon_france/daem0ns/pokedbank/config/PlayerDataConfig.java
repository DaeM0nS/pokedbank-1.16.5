package fr.pixelmon_france.daem0ns.pokedbank.config;

import com.pixelmonmod.pixelmon.api.config.api.data.ConfigPath;
import info.pixelmon.repack.org.spongepowered.objectmapping.ConfigSerializable;
import info.pixelmon.repack.org.spongepowered.objectmapping.meta.Setting;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@ConfigSerializable
@ConfigPath("pokedbank/PlayerData.cfg")
public class PlayerDataConfig {
    @Setting
    public HashMap<UUID, List<String>> playerPokemon = new HashMap<>();

}
