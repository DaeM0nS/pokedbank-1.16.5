package fr.pixelmon_france.daem0ns.pokedbank.database;

import fr.pixelmon_france.daem0ns.pokedbank.PokeBank;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {
    public static final String MYSQL_DB = "jdbc:mysql://" + PokeBank.instance.config.host + ":" + PokeBank.instance.config.port + "/" + PokeBank.instance.config.database;
    public static final String MARIA_DB = "jdbc:mariadb://" + PokeBank.instance.config.host + ":" + PokeBank.instance.config.port + "/" + PokeBank.instance.config.database;
    public static Connection con = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;

    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(MYSQL_DB, PokeBank.instance.config.user, PokeBank.instance.config.password);
            return con;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "SqlException", ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "NullPointerException", ex);
        } catch (ClassNotFoundException e) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection(MYSQL_DB, PokeBank.instance.config.user, PokeBank.instance.config.password);
                return con;
            } catch (SQLException exx) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "SqlException", exx);
            }  catch (NullPointerException exx) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "NullPointerException", exx);
            } catch (ClassNotFoundException exx ) {
                try {
                    Class.forName("org.mariadb.jdbc.Driver");
                    con = DriverManager.getConnection(MARIA_DB, PokeBank.instance.config.user, PokeBank.instance.config.password);
                    return con;
                } catch (SQLException ex1) {
                    Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "SqlException", ex1);
                } catch (ClassNotFoundException ex1) {
                    Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "ClassNotFoundException", ex1);
                } catch (NullPointerException ex1) {
                    Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "NullPointerException", ex1);
                }
            }
        }
        return con;
    }

    public static boolean testCon() {
        if (con == null) {
            return false;
        }
        try {
            if (con.isValid(1)) {
                Logger.getLogger(Database.class.getName()).log(Level.INFO, "[PokeBank]  Mysql database connection successfull");
                DatabaseMetaData dbm = con.getMetaData();
                ResultSet tables = dbm.getTables(null, null, PokeBank.instance.config.table, null);
                if (tables.next()) {
                    return true;
                }
                Logger.getLogger(Database.class.getName()).log(Level.INFO, "[PokeBank]  Table " + PokeBank.instance.config.table + " does not exist");
                return false;
            }
        } catch (SQLException var2) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "[PokeBank]  Could not connect mysql database " + var2);
            return false;
        }
        return false;
    }

    public static Statement getStatement(Connection c) {
        try {
            return c.createStatement();
        } catch (SQLException var2) {
            Logger.getLogger(Database.class.getName()).info("[PokeBank]  Could not create statement for database");
        }
        return null;
    }

    public static Statement getStatement() {
        return getStatement(getConnection());
    }

    public static void finish(Connection c, Statement s) {
        try {
            s.close();
            c.close();
        } catch (SQLException var3) {
            Logger.getLogger(Database.class.getName()).log(Level.INFO, "[PokeBank]  Could finish mysql connection " + var3);
        }
    }

    public static void finish(ResultSet r) {
        try {
            finish(r.getStatement().getConnection(), r.getStatement());
        } catch (SQLException var2) {
            Logger.getLogger(Database.class.getName()).log(Level.INFO, "[PokeBank]  Could finish mysql connection " + var2);
        }
    }

    public static void createTable() throws SQLException {
        try (Connection con = Database.getConnection()) {
            try {
                //check if the table already exists
                Statement statement = con.createStatement();
                statement.execute("CREATE TABLE IF NOT EXISTS `" + PokeBank.instance.config.database + "`.`" + PokeBank.instance.config.table + "` ( `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,  `playername` VARCHAR(32) NOT NULL ,  `uuid` VARCHAR(37) NOT NULL , `pokemon` TEXT NOT NULL, `server` VARCHAR(32) NOT NULL , `locked` BOOLEAN NOT NULL DEFAULT 0, `lastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = InnoDB");
                statement.close();
            } catch (Exception sqlEx) {
                Logger.getLogger(Database.class.getName()).log(Level.INFO, "Table doesn't exist", sqlEx);
            }
        }
    }

    public static void createTableLog() throws SQLException {
        try (Connection con = Database.getConnection()) {

            try {
                //check if the table already exists
                Statement statement = con.createStatement();
                statement.execute("CREATE TABLE IF NOT EXISTS `" + PokeBank.instance.config.database + "`.`" + PokeBank.instance.config.table + "_Log` ( `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,  `playername` VARCHAR(32) NOT NULL , `uuid` VARCHAR(37) NOT NULL , `pokemon` TEXT NOT NULL , `type` VARCHAR(32) NOT NULL , `server` VARCHAR(32) NOT NULL , `lastUpdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ) ENGINE = InnoDB");
                statement.close();

            } catch (Exception sqlEx) {
                Logger.getLogger(Database.class.getName()).log(Level.INFO, "Table doesn't exist", sqlEx);
            }
        }
    }
}


