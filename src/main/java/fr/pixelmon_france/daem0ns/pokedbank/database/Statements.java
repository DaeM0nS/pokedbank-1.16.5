package fr.pixelmon_france.daem0ns.pokedbank.database;

import fr.pixelmon_france.daem0ns.pokedbank.PokeBank;
import net.minecraft.entity.player.ServerPlayerEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class Statements {

    public static boolean exist(String player) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("SELECT `id` FROM " + PokeBank.instance.config.table + " WHERE `playername` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, player);
            ResultSet rs = transactions.executeQuery();

            boolean rv = rs.next();

            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }

            return rv;

        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
        return false;
    }

    public static boolean exist(UUID uuid) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("SELECT `id` FROM " + PokeBank.instance.config.table + " WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, uuid.toString());
            ResultSet rs = transactions.executeQuery();

            boolean rv = rs.next();

            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }

            return rv;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
        return false;
    }

    public static boolean existId(int id) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();

            transactions = con.prepareStatement("SELECT * FROM " + PokeBank.instance.config.table + " WHERE `id` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setInt(1, id);
            ResultSet rs = transactions.executeQuery();
            boolean rv = rs.next();
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
            return rv;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);

        }
        return false;
    }

    public static void setLogger(ServerPlayerEntity player, String pokemon, String type) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("INSERT INTO `" + PokeBank.instance.config.table + "_Log`(`id`, `playername`, `uuid`, `pokemon`, type, `server` ) VALUES (NULL, ? , ? , ? , ? , ?)", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, player.getGameProfile().getName());
            transactions.setString(2, player.getUniqueID().toString());
            transactions.setString(3, pokemon);
            transactions.setString(4, type);
            transactions.setString(5, PokeBank.instance.config.serverName);
            transactions.executeUpdate();
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
            return;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
    }

    public static HashMap<Integer, String> getPlayerPokemon(UUID uuid) {
        HashMap<Integer, String> pokeList = new HashMap<Integer, String>();
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("SELECT * FROM `" + PokeBank.instance.config.table + "` WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, uuid.toString());
            ResultSet rs = transactions.executeQuery();
            while (rs.next()) {
                pokeList.put(rs.getInt("id"), rs.getString("pokemon"));
            }

            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }

        return pokeList;
    }

    public static void addPokemon(ServerPlayerEntity player, String pokemon) {
        PreparedStatement transactions = null;
        Connection con = null;

        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("INSERT INTO `" + PokeBank.instance.config.table + "`(`id`, `playername`, `uuid`, `pokemon`, `server`) VALUES (NULL, ? , ? , ? , ?)", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, player.getGameProfile().getName());
            transactions.setString(2, player.getUniqueID().toString());
            transactions.setString(3, pokemon);
            transactions.setString(4, PokeBank.instance.config.serverName);
            transactions.executeUpdate();
            if (PokeBank.instance.config.useLogTable) {
                Statements.setLogger(player, pokemon, "Store");
            }
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }

            return;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
    }


    public static void removePokemon(ServerPlayerEntity player, String pokemon) {
        PreparedStatement transactions = null;
        Connection con = null;

        try {
            con = Database.getConnection();

            transactions = con.prepareStatement("DELETE FROM `" + PokeBank.instance.config.table + "` WHERE `pokemon` = ? AND `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, pokemon);
            transactions.setString(2, player.getUniqueID().toString());
            transactions.executeUpdate();
            if (PokeBank.instance.config.useLogTable) {
                Statements.setLogger(player, pokemon, "Take");
            }
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }

            return;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
    }

    public static void lockBank(ServerPlayerEntity player) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("UPDATE `" + PokeBank.instance.config.table + "` SET `locked` = ? WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setBoolean(1, true);
            transactions.setString(2, player.getUniqueID().toString());
            transactions.executeUpdate();
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
            return;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
            return;
        }
    }

    public static void unlockBank(ServerPlayerEntity player) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("UPDATE `" + PokeBank.instance.config.table + "` SET `locked` = ? WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setBoolean(1, false);
            transactions.setString(2, player.getUniqueID().toString());
            transactions.executeUpdate();
            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
            return;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
    }

    public static Boolean isLocked(ServerPlayerEntity player) {
        PreparedStatement transactions = null;
        Connection con = null;
        try {
            con = Database.getConnection();
            transactions = con.prepareStatement("SELECT * FROM " + PokeBank.instance.config.table + " WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE);
            transactions.setString(1, player.getUniqueID().toString());
            ResultSet rs = transactions.executeQuery();
            boolean locked = rs.next() && rs.getBoolean("locked");

            if (transactions != null) {
                transactions.close();
            }
            if (con != null) {
                con.close();
            }
            return locked;
        } catch (SQLException var2) {
            if (transactions != null) {
                try {
                    transactions.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[PokeBank] Could not create statement for database " + var2);
        }
        return false;
    }
}